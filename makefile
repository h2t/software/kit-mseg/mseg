# Internal variables
mseg_dir = $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
mseg_home = $${HOME}/.mseg
deb_dist = ./deb-dist

# List of dependencies
.depsdeb:
	@echo armarx-armarxcore
	@echo python-armarx

# Own bashrc entries
.bashrc:
	@printf "# <MSEG>\n"
	@printf "export MSEG_DIR=\"${mseg_dir}\"\n"
	@printf "export MSEG_HOME=\"\${mseg_home}\"\n"
	@printf "# </MSEG>\n\n"

# Changes remote URLs to SSH everywhere; Default is HTTPS
dev:
	@bash ./scripts/https2ssh.sh

# Initialises all repositories
init:
	@# Create home directory
	@mkdir -p ${mseg_home} || true
	@# Clone all repositories
	@bash ./scripts/clone-repos.sh
	@# Symlink ${mseg_home}/motion-data to this datasets repository
	@ln -sn ${mseg_dir}/datasets ${mseg_home}/motion-data 2> /dev/null || true
	@# Check out stable branches
	@bash ./scripts/change-to-stable-refs.sh
	@# Check if all system dependencies are satisfied
	@bash ./scripts/check-deps.sh
	@# Initialise and build the bashrc
	@bash ./scripts/init-mseg-bashrc.sh
	@# Print success message
	@bash -c "source ./scripts/helperfns.sh && print_separator \"Init: FINISHED SUCCESSFULLY\""

# Lists all deb dependencies
depsdeb:
	@bash ./scripts/submake.sh .depsdeb | sort -u

# Lists all pip dependencies
depspip:
	@bash ./scripts/submake.sh .depspip | sort -u

# Lists all pip3 dependencies
depspip3:
	@bash ./scripts/submake.sh .depspip3 | sort -u

bashrc:
	@bash ./scripts/submake.sh .bashrc

# Calls 'clean' on all projects to delete all built artefacts
clean:
	@bash ./scripts/submake.sh clean
	@rm -r ${deb_dist} 2> /dev/null || true

# Calls 'install' on all projects to install them locally
install:
	@bash ./scripts/submake.sh install

# Calls 'package' on all projects to generate .deb files. The .deb
# files will be collected afterwards and copied to ${deb_dist}
package:
	@rm -r ${deb_dist} 2> /dev/null || true
	@mkdir ${deb_dist} 2> /dev/null || true
	@bash ./scripts/submake.sh package
	@# Collect .deb files
	@mv ./**/deb-dist/*.deb ${deb_dist}
