#!/bin/bash

set -e

# Source helper functions
source ./scripts/helperfns.sh

(cd core && git_remote_to_ssh "core")
(cd core-ice && git_remote_to_ssh "core-ice")
(cd pli-cpp && git_remote_to_ssh "pli-cpp")
(cd pli-java && git_remote_to_ssh "pli-java")
(cd pli-python && git_remote_to_ssh "pli-python")
(cd pli-matlab && git_remote_to_ssh "pli-matlab")
(cd mseg-tools && git_remote_to_ssh "mseg-tools")
(cd datasets && git_remote_to_ssh "datasets")
(cd documentation && git_remote_to_ssh "documentation")
(cd dummy-packages && git_remote_to_ssh "dummy-packages")
