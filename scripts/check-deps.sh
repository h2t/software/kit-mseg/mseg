#!/bin/bash

set -e

# Source helper functions
source ./scripts/helperfns.sh

deps_error="ERROR: Dependencies not satisfied. To install them, please run"

print_separator "Checking dependencies"

echo "Checking deb dependencies"

# Make sure that deb dependencies are installed
(make --no-print-directory depsdeb | xargs dpkg -l > /dev/null 2>&1) \
    || (echo "${deps_error} \`make depsdeb | xargs sudo apt install -y\`. This may download up to 1.1 GiB of data"; false)

echo "Checking pip dependencies"

# Make sure that pip dependencies are installed
(make --no-print-directory depspip | xargs -I {} sh -c 'pip list | fgrep {} > /dev/null;') \
    || (echo "${deps_error} \`make depspip | xargs sudo pip install\`"; false)

echo "Checking pip3 dependencies"

# Make sure that pip3 dependencies are installed
(make --no-print-directory depspip3 | xargs -I {} sh -c 'pip3 list | fgrep {} > /dev/null;') \
    || (echo "${deps_error} \`make depspip3 | xargs sudo pip3 install\`"; false)

echo "All reported deb, pip and pip3 dependencies are installed"
