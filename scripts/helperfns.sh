#!/bin/bash

set -e

# Prints a prominent separator (needed due to building tools verbosity)
function print_separator {
    if [ "$2" = true ]; then
        return 0
    fi

    local c="\033[1;36m"
    local b="\033[0;36m"
    local d="\033[0;34m"
    local w="\033[0m"

    printf "\n"
    printf "$c====================================================$b=$d-$w\n"
    printf "$c==$b=$d-$w                                            $d-$b=$c=$b=$d-$w\n"
    printf "$c====$b=$d-$w    $1\n"
    printf "$c==$b=$d-$w                                          $d-$b=$c=$b=$d-$w\n"
    printf "$c================================================$b=$d-$w\n"
    printf "\n"
}

# Clones the given repository
function git_clone_mseg_repo {
    git clone --progress "https://gitlab.com/h2t/kit-mseg/$1.git" && echo ""
    return $?
}

# Replaces the remote URL with the SSH path instead of the HTTPS
function git_remote_to_ssh {
    git remote set-url origin "git@gitlab.com:h2t/kit-mseg/$1.git"
    return $?
}

# Run a make command if the rule exists, else return cleanly without doing anything
function make_failsafe {
    local dir=$1
    local rule=$2

    # If the rule exists in the makefile
    if (cd ${dir} && grep "^${rule}:" makefile > /dev/null); then
        make --no-print-directory --directory ${dir} ${rule}
    fi

    return 0
}
